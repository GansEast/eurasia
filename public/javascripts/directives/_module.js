'use strict';

/* Directives */

var testDirectives = angular.module('testDirectives', []);

testDirectives
  .directive('backImg',  require('./back-img'))
  .directive('backImg2',  require('./back-img2'))
