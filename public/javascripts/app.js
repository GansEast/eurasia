'use strict';

// var angular = require('angular');
//               require('angular-resource');
//               require('angular-sanitize');
              // require('angular-swipe');
              // require('angular-ui-router');
              // require('ng-dialog');
              // require('angular-scroll');
              // var Flickity = require('./vendor/flickity.pkgd.min');
              require('angular-flickity');
              require('./controllers/_module');
              require('./directives/_module');
              require('./services/_module');
angular
  .module('test', [
    'ngDialog',
    'ngResource',
    'ngSanitize',
    'ui.router',
    'duScroll',
    'bc.Flickity',

    'testControllers',
    'testServices',
    'testDirectives'
  ]).value('duScrollGreedy', true).value('duScrollDuration', 800)
  .run(['$rootScope', '$anchorScroll', '$state', function($rootScope, $anchorScroll, $state){
    $rootScope.$on('$stateChangeStart', function(e, toState, toParams, fromState, fromParams) {
      $anchorScroll();
    });
    $rootScope.$state = $state;
  }])

  .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.
      state('project', {
        url: '/',
        templateUrl: 'templates/project',
        data: {
          title: 'Проект'
        }
      }).
      state('processes', {
        url: '/process/',
        templateUrl: 'templates/processes',
        controller: 'processesCtrl',
        data: {
          title: 'Процесс'
        }
      }).
      state('process', {
        url: '/process/:id',
        templateUrl: 'templates/process',
        controller: 'processCtrl',
        data: {
          title: 'Процесс'
        }
      }).
      state('team', {
        url: '/team/:id',
        templateUrl: 'templates/team',
        controller: 'peopleCtrl',
        data: {
          title: 'Процесс'
        }
      }).
      state('online', {
        url: '/online/',
        templateUrl: 'templates/online',
        data: {
          title: 'Онлайн'
        }
      });
      $urlRouterProvider.otherwise('/');
      $locationProvider.html5Mode(true);
  }])