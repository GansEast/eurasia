exports = module.exports = ['$scope', function ($scope) {
  $scope.myCustomOptions = {
    cellSelector: '.team-slide',
    autoPlay: true,
    pageDots: false,
    wrapAround: true,
    contain: true
  };
}];